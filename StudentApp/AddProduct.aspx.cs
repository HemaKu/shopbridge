﻿using BusinessLayer;
using EntityLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProductApp
{
    public partial class AddProduct : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
            string Opertaion = Request["Operation"].ToString();
            if (Opertaion == "Add")
            {
                Submit.Visible = true;
                Update.Visible = false;
            }
            if (Opertaion == "Edit")
            {
                if (!IsPostBack)
                {
                    Submit.Visible = false;
                    Update.Visible = true;
                    string Id = Request["Id"].ToString();
                    Product_BAL ProObj = new Product_BAL();
                    ProductModel model = ProObj.ProductSingleSelect(Convert.ToInt32(Id));
                    TxtName.Text = model.Product_Name;
                    TxtDesc.Text = model.Product_Desc;
                    TxtPrice.Text = model.Product_Price.ToString();
                    TxtQuantity.Text = model.Product_Quantity.ToString();
                    hdnImagePath.Value = model.Product_ImagePath;
                    HiddenProductid.Value = Id;
                }
            }

        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            Product_BAL prodObj = new Product_BAL();
            ProductModel model = new ProductModel();
            model.Product_Name = TxtName.Text;
            model.Product_Quantity = Convert.ToInt32(TxtQuantity.Text);
            model.Product_Desc = TxtDesc.Text;
            model.Product_Price = Convert.ToDecimal(TxtPrice.Text);
            if (FileUpload.HasFile)
            {
                string FolderPath = Server.MapPath("~/Files/");
                if (!Directory.Exists(FolderPath))
                {
                    Directory.CreateDirectory(FolderPath);
                }
                string FIlePtah = FolderPath + Path.GetFileNameWithoutExtension(FileUpload.FileName) + DateTime.Now.Ticks + Path.GetExtension(FileUpload.FileName);
                FileUpload.SaveAs(FIlePtah);
                model.Product_ImagePath = FIlePtah;
            }
            prodObj.AddProduct(model);
            Response.Redirect("ProductList.aspx");
        }

        protected void Update_Click(object sender, EventArgs e)
        {
            Product_BAL prodObj = new Product_BAL();
            ProductModel model = new ProductModel();
            model.Product_Name = TxtName.Text;
            model.Product_Quantity =Convert.ToInt32( TxtQuantity.Text);
            model.Product_Desc = TxtDesc.Text;
            model.Product_Price = Convert.ToDecimal(TxtPrice.Text);
            model.Product_Id = Convert.ToInt32(HiddenProductid.Value);
            if (FileUpload.HasFile)
            {
                string FolderPath = Server.MapPath("~/Files/");
                if (!Directory.Exists(FolderPath))
                {
                    Directory.CreateDirectory(FolderPath);
                }
                string FIlePtah = FolderPath + Path.GetFileNameWithoutExtension(FileUpload.FileName) + DateTime.Now.Ticks + Path.GetExtension(FileUpload.FileName);
                FileUpload.SaveAs(FIlePtah);
                model.Product_ImagePath = FIlePtah;
            }
            else
            {
                model.Product_ImagePath = hdnImagePath.Value;
            }
            prodObj.UpdateProduct(model);
            Response.Redirect("ProductList.aspx");
        }
    }
}