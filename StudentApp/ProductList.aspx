﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProductList.aspx.cs" Inherits="ProductApp.ProductList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Button runat="server" ID="AddProduct" Text="Add New" OnClick="AddProduct_Click" />
            <asp:Label runat="server" ID="ErrorMsg"></asp:Label>
            <asp:GridView ID="ProductListGridView" PageSize="5"  runat="server" AutoGenerateColumns="false" OnRowCommand="ProductListGridView_RowCommand" OnPageIndexChanging="ProductListGridView_PageIndexChanging" OnRowDataBound="ProductListGridView_RowDataBound">
                <Columns>
                    <asp:BoundField HeaderText="Name" DataField="Product_Name" />
                    <asp:BoundField HeaderText="Desc" DataField="Product_Desc" />
                    <asp:BoundField HeaderText="Price" DataField="Product_Price" />
                    <asp:BoundField HeaderText="Quantity" DataField="Product_Quantity" />
                    <asp:TemplateField HeaderText="Action">
                        <ItemTemplate>
                            <asp:Button runat="server" ID="BtnEditProduct" Text="Edit" CommandName="EditProduct" CommandArgument='<%#Eval("Product_Id")%>'/>
                            <asp:Button runat="server" ID="ButDeleteProduct" Text="Delete" CommandName="DeleteProduct" CommandArgument='<%#Eval("Product_Id")%>'/>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </form>
</body>
</html>
