﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddProduct.aspx.cs" Inherits="ProductApp.AddProduct" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table>

                <asp:HiddenField ID="hdnImagePath" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="HiddenProductid" runat="server"></asp:HiddenField>
                <tr>
                    <td>Name</td>
                    <td>
                        <asp:TextBox ID="TxtName" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Desc</td>
                    <td>
                        <asp:TextBox ID="TxtDesc" runat="server" TextMode="MultiLine"></asp:TextBox>
                       </td>
                </tr>
                
                <tr>
                    <td>Price</td>
                    <td>
                        <asp:TextBox ID="TxtPrice" runat="server" TextMode="Number"></asp:TextBox>
                    </td>
                </tr>
                <tr>

                    <td>Quantity</td>
                    <td>
                        <asp:TextBox ID="TxtQuantity" runat="server"  TextMode="Number"></asp:TextBox>
                        </td>
                </tr>
                <tr>
                    <td>Image</td>
                    <td>
                        <asp:FileUpload runat="server" ID="FileUpload"  /></td>
                </tr>
                <tr>
                    <td>
                        <asp:Button runat="server" ID="Submit" ValidationGroup="save" Text="Save" CausesValidation="true" OnClick="Submit_Click" />
                        <asp:Button runat="server" ID="Update" Visible="false" Text="Update" CausesValidation="true" OnClick="Update_Click" />
                    </td>
                </tr>
            </table>

        </div>
    </form>
</body>
</html>
