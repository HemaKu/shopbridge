﻿using BusinessLayer;
using EntityLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProductApp
{
    public partial class ProductList : System.Web.UI.Page
    {
        Product_BAL balObj = new Product_BAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindProductList();
            }
        }

        public void BindProductList()
        {

            List<ProductModel> slist = new List<ProductModel>();
            slist = balObj.ProductList();
            ProductListGridView.DataSource = slist;
            ProductListGridView.DataBind();

        }

        
        protected void AddProduct_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddProduct.aspx?Operation=Add");
        }

        protected void ProductListGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditProduct")
            {
                Response.Redirect("AddProduct.aspx?Operation=Edit&Id=" + e.CommandArgument.ToString());
            }
            if (e.CommandName == "DeleteProduct")
            {
                bool issuccess = balObj.DeleteProduct(Convert.ToInt32(e.CommandArgument.ToString()));

                if (issuccess)
                {
                    BindProductList();
                    ErrorMsg.Text = "Product deleted successfully.";
                    ErrorMsg.ForeColor = System.Drawing.Color.Red;
                }
            }
        }

        protected void ProductListGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ProductListGridView.PageIndex = e.NewPageIndex;
            BindProductList();
        }

        protected void ProductListGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }
    }
}