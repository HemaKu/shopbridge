﻿using BusinessLayer;
using EntityLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace ProductMvcApp.Controllers
{
    public class ProductController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
            List<ProductModel> plist = new List<ProductModel>();
            Product_BAL p = new Product_BAL();
            plist = p.ProductList();
            return View(plist);
        }
        public ActionResult DeleteProduct(int id)
        {
            List<ProductModel> plist = new List<ProductModel>();
            Product_BAL p = new Product_BAL();
            plist = p.ProductList();
            bool issuccess = p.DeleteProduct(id);
            return RedirectToAction("Index", "Product");
        }
        public ActionResult AddProduct()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddProduct(ProductModel model)
        {
            Product_BAL pObj = new Product_BAL();
            if (HttpContext.Request.Files.Count > 0)
            {
                string FolderPath = Server.MapPath("~/Files/");
                if (!Directory.Exists(FolderPath))
                {
                    Directory.CreateDirectory(FolderPath);
                }
                string FIlePtah = FolderPath + Path.GetFileNameWithoutExtension(HttpContext.Request.Files[0].FileName) + DateTime.Now.Ticks + Path.GetExtension(HttpContext.Request.Files[0].FileName);
                HttpContext.Request.Files[0].SaveAs(FIlePtah);
                model.Product_ImagePath = FIlePtah;
            }
            pObj.AddProduct(model);
            return RedirectToAction("Index", "Product");
        }
        public ActionResult EditProduct(int id)
        {
            ProductModel model = new ProductModel();
            Product_BAL p = new Product_BAL();
            model = p.ProductSingleSelect(id);
            return View(model);
        }
        [HttpPost]
        public ActionResult EditProduct(ProductModel model)
        {
           
            Product_BAL pObj = new Product_BAL();
            if (HttpContext.Request.Files.Count>0)
            {
                string FolderPath = Server.MapPath("~/Files/");
                if (!Directory.Exists(FolderPath))
                {
                    Directory.CreateDirectory(FolderPath);
                }
                string FIlePtah = FolderPath + Path.GetFileNameWithoutExtension(HttpContext.Request.Files[0].FileName) + DateTime.Now.Ticks + Path.GetExtension(HttpContext.Request.Files[0].FileName);
                HttpContext.Request.Files[0].SaveAs(FIlePtah);
                model.Product_ImagePath = FIlePtah;
            }
            pObj.UpdateProduct(model);
            return RedirectToAction("Index", "Product");
        }
        public ActionResult EditStudent(int id)
        {
            ProductModel plist = new ProductModel();
            Product_BAL p = new Product_BAL();
            plist = p.ProductSingleSelect(id);
            return View(plist);
        }
    }
}