﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace EntityLayer
{
    public class ProductModel
    {
        public int Product_Id { get; set; }
        public string Product_Name { get; set; }
        public string Product_Desc { get; set; }
        public decimal? Product_Price { get; set; }
        public int? Product_Quantity { get; set; }
        public string Product_ImagePath { get; set; }
        public HttpPostedFile file { get; set; }
    }
}
