Create DATABASE [SampleDBHema] 
GO

CREATE TABLE [dbo].[Product](
	[Product_Id] [int] IDENTITY(1,1) NOT NULL,
	[Product_Name] [varchar](max) NULL,
	[Product_Desc] [varchar](max) NULL,
	[Product_Price] [decimal](18, 2) NULL,
	[Product_Quantity] [int] NULL,
	[Product_ImagePath] [varchar](max) NULL,
 CONSTRAINT [PK__Student__A2F4E98CBE36CBC2] PRIMARY KEY CLUSTERED 
(
	[Product_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Product] ON 

GO
INSERT [dbo].[Product] ([Product_Id], [Product_Name], [Product_Desc], [Product_Price], [Product_Quantity], [Product_ImagePath]) VALUES (2, N'TestEmployee', N'dfetrdfgdfgdfg', CAST(4.00 AS Decimal(18, 2)), 45, N'D:\ShopBridgeApp\StudentMvcApp\Files\637714956820042986')
GO
SET IDENTITY_INSERT [dbo].[Product] OFF
GO
USE [master]
GO
ALTER DATABASE [SampleDBHema] SET  READ_WRITE 
GO
