﻿using DataAccessLayer;
using EntityLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class Product_BAL
    {
        SampleDBContext context = new SampleDBContext();
        public List<ProductModel> ProductList()
        {
            List<ProductModel> plist = new List<ProductModel>();
            plist = (from p in context.Products
                     select new ProductModel()
                     {
                         Product_Id = p.Product_Id,
                         Product_Name = p.Product_Name,
                         Product_Price = p.Product_Price,
                         Product_Quantity = p.Product_Quantity,
                         Product_ImagePath = p.Product_ImagePath,
                         Product_Desc=p.Product_Desc
                     }).ToList();
            return plist;
        }
        public bool DeleteProduct(int ProductId)
        {
            var product = context.Products.Where(x => x.Product_Id == ProductId).FirstOrDefault();
            context.Products.Remove(product);
            context.SaveChanges();
            return true;
        }
        public ProductModel ProductSingleSelect(int ProductId)
        {
            ProductModel student = (from p in context.Products
                                    where p.Product_Id == ProductId
                                    select new ProductModel()
                                    {

                                        Product_Id = p.Product_Id,
                                        Product_Name = p.Product_Name,
                                        Product_Price = p.Product_Price,
                                        Product_Quantity = p.Product_Quantity,
                                        Product_ImagePath = p.Product_ImagePath,
                                        Product_Desc = p.Product_Desc
                                    }).FirstOrDefault();
            return student;
        }
       
        public bool UpdateProduct(ProductModel model)
        {
            var p = context.Products.FirstOrDefault(x => x.Product_Id == model.Product_Id);
            p.Product_Name = model.Product_Name;
            p.Product_Desc = model.Product_Desc;
            p.Product_ImagePath = model.Product_ImagePath;
            p.Product_Quantity = model.Product_Quantity;
            p.Product_Price = model.Product_Price;
            context.SaveChanges();
            return true;
        }
        public bool AddProduct(ProductModel model)
        {
            Product p = new Product();
            p.Product_Name = model.Product_Name;
            p.Product_Desc = model.Product_Desc;
            p.Product_ImagePath = model.Product_ImagePath;
            p.Product_Quantity = model.Product_Quantity;
            p.Product_Price = model.Product_Price;
            context.Products.Add(p);
            context.SaveChanges();
            return true;
        }
    }
}
